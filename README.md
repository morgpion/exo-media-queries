
## Exercice sur les media queries ##

## Installation Bootstrap ##

*  **Installation des fichiers** : Télécharger à l'adresse https://getbootstrap.com/docs/4.5/getting-started/download/ dans la section "**Compiled CSS and JS**".

*  **Accès via Content Delivery Network (CDN)** : intégrer les &lt;link&gt; selon les instructions à l'adresse https://getbootstrap.com/docs/4.5/getting-started/download/ dans la section "**BootstrapCDN**".

*  **Astuce VSCode** : Installez le snipet "**Bootstrap 4 CDN**" et à la 1è ligne du fichier tapez "!bcdn" + Tab ou Entrée. Tout le &lt;head&gt; s'affichera avec les liens CDN de Bootstrap : css, js, jquery et font-awesome5.


Aucune autre technologie spécifique utilisée pour cet exercice.



